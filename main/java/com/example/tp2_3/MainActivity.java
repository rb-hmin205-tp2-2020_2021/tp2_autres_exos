package com.example.tp2_3;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MainActivity extends AppCompatActivity implements SensorEventListener{

    private boolean flash=false;
    private SensorManager sensorMgr;
    private Sensor sensor;
    private static final int SHAKE_THRESHOLD = 400;
    private CameraManager cameraManager;
    //@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorMgr = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorMgr.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION);
        Boolean supported = sensorMgr.registerListener(this,sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_UI);
        sensorMgr.registerListener(this,sensorMgr.getDefaultSensor(Sensor.TYPE_PROXIMITY),SensorManager.SENSOR_DELAY_UI);
        if (!supported) {
            sensorMgr.unregisterListener
                    ( this,sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
            //((TextView)findViewById(R.id.acc)).setText("Pas d’accelerometre");
            Log.v("SensorActivity", "Pas d’accelerometre");
        }
        cameraManager = (CameraManager) getSystemService(CAMERA_SERVICE);
        if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)){
            if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)){

            }
            else
            {
                Toast.makeText(this,"This device has no flash",Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this,"This device has no camera",Toast.LENGTH_SHORT).show();
        }
    }

    long lastUpdate=System.currentTimeMillis();
    float x,y,z;
    float last_x= 0,last_y =0,last_z=0;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onSensorChanged(SensorEvent event) {


        switch(event.sensor.getType()){
            case Sensor.TYPE_ACCELEROMETER:
                onAccelerometerChanged(event);
                break;
            case Sensor.TYPE_PROXIMITY:
                onProximityChanged(event);
                break;
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void onAccelerometerChanged(SensorEvent event) {

        float gravity[]=new float[3];
        float linear_acceleration[]=new float[3];
        final float alpha = 0.8f;


        ConstraintLayout rl=(ConstraintLayout)findViewById(R.id.n);
        // Isolate the force of gravity with the low-pass filter.
        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

        // Remove the gravity contribution with the high-pass filter.


        linear_acceleration[0] = event.values[0] - gravity[0];
        linear_acceleration[1] = event.values[1] - gravity[1];
        linear_acceleration[2] = event.values[2] - gravity[2];


        //System.out.println("Oy ! "+linear_acceleration[0]+" "+linear_acceleration[1]+" "+linear_acceleration[2]);
        ((TextView)findViewById(R.id.textView)).setText("Axe X: "+linear_acceleration[0]);
        ((TextView)findViewById(R.id.textView)).setTextColor(Color.WHITE);
        ((TextView)findViewById(R.id.textView2)).setText("Axe Y: "+linear_acceleration[1]);
        ((TextView)findViewById(R.id.textView2)).setTextColor(Color.WHITE);
        ((TextView)findViewById(R.id.textView3)).setText("Axe Z: "+linear_acceleration[2]);
        ((TextView)findViewById(R.id.textView3)).setTextColor(Color.WHITE);

        ((TextView)findViewById(R.id.prox)).setTextColor(Color.WHITE);

        ((TextView)findViewById(R.id.textView4)).setText("");
        ((TextView)findViewById(R.id.textView4)).setTextColor(Color.WHITE);

        if(linear_acceleration[0]>0.5)
        {
            ((TextView)findViewById(R.id.textView4)).setText("Mouvement vers la droite");
        }
        else if(linear_acceleration[0]<-0.5)
        {
            ((TextView)findViewById(R.id.textView4)).setText("Mouvement vers la gauche");
        }
        if(linear_acceleration[2]>0.5)
        {
            ((TextView)findViewById(R.id.textView4)).setText("Mouvement vers l'avant");
        }
        else if(linear_acceleration[2]<-0.5)
        {
            ((TextView)findViewById(R.id.textView4)).setText("Mouvement vers l'arrière'");
        }
        if((linear_acceleration[1]>7.80)&&(linear_acceleration[1]<8.00))
            rl.setBackgroundColor(Color.BLACK);
        else
        {
            if(linear_acceleration[1]>=8.00) {
                rl.setBackgroundColor(Color.RED);
                ((TextView)findViewById(R.id.textView)).setTextColor(Color.BLACK);
                ((TextView)findViewById(R.id.textView2)).setTextColor(Color.BLACK);
                ((TextView)findViewById(R.id.textView3)).setTextColor(Color.BLACK);
                ((TextView)findViewById(R.id.textView4)).setTextColor(Color.BLACK);
                ((TextView)findViewById(R.id.prox)).setTextColor(Color.BLACK);
            }
            else {
                rl.setBackgroundColor(Color.GREEN);
                ((TextView)findViewById(R.id.textView)).setTextColor(Color.BLACK);
                ((TextView)findViewById(R.id.textView2)).setTextColor(Color.BLACK);
                ((TextView)findViewById(R.id.textView3)).setTextColor(Color.BLACK);
                ((TextView)findViewById(R.id.textView4)).setTextColor(Color.BLACK);
                ((TextView)findViewById(R.id.prox)).setTextColor(Color.BLACK);
            }
        }
        long curTime = System.currentTimeMillis();

        // only allow one update every 100ms.
        if ((curTime - lastUpdate) > 100) {
            long diffTime = (curTime - lastUpdate);
            lastUpdate = curTime;

            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            float speed = Math.abs(x+y+z - last_x - last_y - last_z) / diffTime * 10000;

            if (speed > SHAKE_THRESHOLD) {
                Log.d("sensor", "shake detected w/ speed: " + speed);
                Toast.makeText(this, "shake detected w/ speed: " + speed, Toast.LENGTH_SHORT).show();
                if(!flash) {
                    try {
                        cameraManager.setTorchMode("0", true);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    try {
                        cameraManager.setTorchMode("0", false);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }


            }
            last_x = x;
            last_y = y;
            last_z = z;
        }

    }
    private void onProximityChanged(SensorEvent event){
        float distance;
        distance = event.values[0];
        String uri = "@android:drawable/stat_sys_download";
        String uri1 = "@android:drawable/stat_sys_upload";
        int imageResource1 = getResources().getIdentifier(uri, null, getPackageName());
        int imageResource2 = getResources().getIdentifier(uri1, null, getPackageName());
        ((TextView)findViewById(R.id.prox)).setText("Proximity: " + distance + "cm");
        if(distance>5)
        {
            ((ImageView)findViewById(R.id.imageView)).setImageDrawable(getResources().getDrawable(imageResource1));
        }
        else
        {
            ((ImageView)findViewById(R.id.imageView)).setImageDrawable(getResources().getDrawable(imageResource2));
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    /*public void onSensorChanged(SensorEvent event){
        // In this example, alpha is calculated as t / (t + dT),
        // where t is the low-pass filter's time-constant and
        // dT is the event delivery rate.

    }*/
    @Override
    protected void onResume() {
        super.onResume();
        sensorMgr.registerListener(this, sensor,
                SensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override
    protected void onPause() {
        super.onPause();
        sensorMgr.unregisterListener(this);
    }
}